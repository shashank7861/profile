import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './_shared/header/header.component';
import { BannerComponent } from './components/banner/banner.component';
import { HomeComponent } from './components/home/home.component';
import { PersonalComponent } from './components/personal/personal.component';
import { MaterialModule } from '../material';
import { GraphsModule } from './graphs/graphs.module';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { OpeningComponent } from './components/opening/opening.component';
import { SkillsComponent } from './components/skills/skills.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ContactComponent } from './components/contact/contact.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    HomeComponent,
    PersonalComponent,
    OpeningComponent,
    SkillsComponent,
    ContactComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    GraphsModule,
    HttpClientModule,
    DragDropModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'x-csrf',
    }),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
