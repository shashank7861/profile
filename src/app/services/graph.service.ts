import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GraphService {
  private httpOptions: HttpHeaders;
  selectedNodeId = 0;
  nodeSelection = 0;
  apiKey = 'c889p72ad3iet0qjdjf0';
  constructor(
    private http: HttpClient,
    private router: Router,
    private snackBar: MatSnackBar,
    ) {
    this.httpOptions = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    let headers = new HttpHeaders()
    headers = headers.set('TRN-Api-Key', this.apiKey)
  }

  getStats = (): Observable<any> => {
    const tempURL = 'https://finnhub.io/api/v1/stock/candle?symbol=IBM&resolution=D&from=1572651390&to=1575243390';
    // const StatsEndpoint = 'https://finnhub.io/api/v1/search';
    return this.http.get(tempURL,{
      headers: this.httpOptions,
    }).pipe(
      map((response: any) => {
        console.log(response);
        return response;
      }),
      catchError((error) => throwError(error))
    );
  };


  getRandomNumber = (min:number,max:number) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  getRandomArray = (length:number,max:any) => {
    return Array.apply(null, Array(length)).map(function() {
      return Math.round(Math.random() * max);
    });
  }


  // getBatchList = (
  //   page,
  //   size,
  //   status,
  //   search,
  //   start_date,
  //   end_date,
  //   displayName,
  //   label
  // ): Observable<any> => {
  //   const options = {
  //     params: new HttpParams()
  //       .set('page', page)
  //       .set('page_size', size)
  //       .set('status', status)
  //       .set('q', search)
  //       .set('start_date', start_date)
  //       .set('end_date', end_date),

  //   };
  //   label.forEach((item) => {
  //     options.params = options.params.append('label[]', item);
  //   });
  //   const BatchListEndpoint = this.globals.urlJoinWithParam(
  //     'home',
  //     'batchList',
  //     displayName
  //   );
  //   return this.http.get(BatchListEndpoint, options).pipe(
  //     map((response: any) => {
  //       return response;
  //     }),
  //     catchError((error) => throwError(error))
  //   );
  // };



}
