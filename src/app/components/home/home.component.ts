import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public now: Date = new Date();
  public href: string = "";
  constructor(private router: Router){
    setInterval(() => {
      this.now = new Date();
    }, 1);
  }

  ngOnInit() {
      this.href = this.router.url;
      console.log(this.router.url);
  }

  goToTop = () =>{
    window.open('#home',"_self");
  }
}
