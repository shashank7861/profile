import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  activeSkill = null;
  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
  ) {
    this.iconRegistry.addSvgIcon(
      'angular',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/angular.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'html',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/html.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'css',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/css.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'js',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/js.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'python',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/python.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'nodejs',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/nodejs.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'server',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/server.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'mongo',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/mongo.svg'
      )
    );
    this.iconRegistry.addSvgIcon(
      'docker',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icon/docker.svg'
      )
    );

   }

  ngOnInit(): void {
  }

  activateSkill = (skill:any) => {
    console.log('Skill' +skill);
    this.activeSkill = skill;
  }

  deactivateSkill = () => {
    this.activeSkill = null;
  }

}
