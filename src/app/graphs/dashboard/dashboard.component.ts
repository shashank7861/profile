import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public href: string = "";
  constructor(private router: Router){
  }

  ngOnInit() {
      this.href = this.router.url;
      console.log(this.router.url);
  }

  goToTop = () =>{
    window.open('#home',"_self");
  }

}
