import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { GraphService } from 'src/app/services/graph.service';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  data1:any = [];
  data2:any = [];
  data3:any = [];
  constructor(
    private graphService: GraphService
  ) { }

  ngOnInit(): void {
    this.getData();
    this.drawHighChart();
  }
  chartOptions = {};

  getData = () => {
    // this.graphService.getStats().subscribe(res => {
    //   if (res) {
    //     console.log(res);
    //   }
    // });
    this.data1 = this.graphService.getRandomArray(10,100);
    this.data2 = this.graphService.getRandomArray(10,100);
    this.data3 = this.graphService.getRandomArray(10,100);
    this.drawHighChart();

  }
  // addPoint = () =>{
  //   const min=100;
  //   const max=1000;
  //   let x:any = Math.floor(Math.random()*(max-min+1)+min);
  //   this.data1.push(x);
  //   this.drawHighChart();
  // }

  private drawHighChart = () => {
    this.chartOptions = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'column'
      },
      title: {
          text: '',

      },
      credits: {
        enabled: false
      },
      tooltip: {
          pointFormat: '{series.name}: {point.y}%'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
      plotOptions: {
        column: {
            zones: [{
                // value: 10,
                // color: 'blue'
            },{
                // color: 'red' // Values from 10 (including) and up have the color red
            }]
        }
      },
      series: [
        {
          name: '2020',
          color: '#607d8b',
          // colorByPoint: true,
          data: this.data1
        },
        {
          name: '2021',
          color: '#00838f',
          // colorByPoint: true,
          data: this.data2
        },
        {
          name: '2022',
          color: '#f9a825',
          // colorByPoint: true,
          data: this.data3
        }
      ]
    };
  }




}
