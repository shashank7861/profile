import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SplineComponent } from './spline/spline.component';
import { ColumnComponent } from './column/column.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { PieComponent } from './pie/pie.component';
import { MaterialModule } from 'src/material';

@NgModule({
  declarations: [
    SplineComponent,
    ColumnComponent,
    DashboardComponent,
    PieComponent,
  ],
  imports: [
    CommonModule,
    HighchartsChartModule,
    MaterialModule
  ]
})
export class GraphsModule { }
